$(document).ready(function () {
    $('[data-toggle]').on('click', function (e) {
        e.preventDefault();
        var boxName = $(this).attr('data-toggle');
        $('[data-open="' + boxName + '"]').toggleClass('active');
    });

    $('.custom-select-3 .field').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $(this).next('.dropdown').slideToggle(200).toggleClass('active');
    });

    //Filtre mobile açma kapama
    $('.mobile-filter-select').on('click', function () {
        $(this).toggleClass('active');
        $(this).next('.mobile-filter-select-list').slideToggle(200).toggleClass('active');
    });


    //Mobile menu toggle
    var $menuArrow = $('.header-nav .mobile-menu-arrow');

    $menuArrow.on('click', function () {
        var $parentLi = $(this).parent('li');

        $('.header-nav .header-nav__sub-menu').not($(this).parent().children('.header-nav__sub-menu')).slideUp(200);
        $menuArrow.parent('li').not($parentLi).removeClass('active');
        $parentLi.toggleClass('active').children('.header-nav__sub-menu').slideToggle(200);
    });

    //Resize olunca mobile menüdeki activeleri temizle
    $(window).on('resize', function () {
        $('.header-nav li').removeClass('active');
    });

    //thumbnail slider max item control
    var pageSize = 2;
    if ($(window).width() > 1400) {
        pageSize = 9;
    } else if ($(window).width() > 1200) {
        pageSize = 7;
    } else if ($(window).width() > 768) {
        pageSize = 5;
    } else if ($(window).width() > 500) {
        pageSize = 3;
    } else {
        pageSize = 1;
    }
    $('.flexslider.flex-direction:not(.mobile-slider)').flexslider({
        animation: "slide",
        animationLoop: false,
        controlNav: false,
        itemWidth: 150,
        itemMargin: 5,
        minItems: 2,
        maxItems: pageSize,
        move: 1
    });
    if($(window).width() < 767) {
        $('.flexslider.flex-direction.mobile-slider').flexslider({
            animation: "slide",
            animationLoop: false,
            controlNav: false,
            itemWidth: 150,
            itemMargin: 5,
            minItems: 2,
            maxItems: pageSize,
            move: 1
        });
    }

    //Normal tekli slider için
    $('.flexslider.single-slide').flexslider({
        animation: "slide",
        animationLoop: false,
        controlNav: false
    });

    //Normal tekli slider için
    $('.flexslider.fade-slide').flexslider({
        animation: "fade",
        animationLoop: false,
        controlNav: true
    });

    //Ürün detay slider
    $('.product-detail-slider').flexslider({
        animation: "fade",
        slideshow: false,
        controlNav: "thumbnails",
        directionNav: false,
        pauseOnHover: true,
        animationSpeed: 10,
        slideshowSpeed: 10,
        controlsContainer: ".product-detail-slide-thumbs"
    });

    //scroll event
    if ($('.header-nav').length) {
        var headerNavTop = $('.header-nav').offset().top - 20;
    }
    $(window).on('scroll', function () {
        var scrollTop = $(window).scrollTop();

        if ($('.header-nav').length) {
            if (scrollTop > headerNavTop && $(window).width() > 990) {
                $('header').addClass('fixed');
            } else {
                $('header').removeClass('fixed');
            }
        }

        if (scrollTop > 10) {
            $('#back-to-top').show();
        } else {
            $('#back-to-top').hide();
        }

    });

    //Yukarıya git click event
    $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        window.scrollTo(0,0);
    });

    //mobile search box animasyon
    $('.mobile-search .toggle-button, .mobile-search .close-button').on('click', function () {
        $('.mobile-search-box').slideToggle(200);
    });

    //tab
    $('.tab .tabs a').on('click', function (e) {
        e.preventDefault();
        var $this = $(this),
            id = $this.attr('href');
        $this.parents('.tab').find('.tabs a, .tab-content .content').removeClass('active');
        $this.addClass('active');
        $(id).addClass('active')
    })

});